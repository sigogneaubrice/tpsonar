package EKIP.TP3;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Courriel {

	//------- ATTRIBUTS --------
	
	private String adresseDest;
	private String titre;
	private String corps;
	private ArrayList<String> piecesJointes;
	
	
	//------ CONSTRUCTEURS ------
	
	public Courriel() {
		
		this.adresseDest = "nom.exemple@gmail.com";
		this.titre = "";
		this.corps = "";
		this.piecesJointes = new ArrayList<>();
	}
	
	public Courriel(String adresseDest, String titre, String corps) {
		
		this.adresseDest = adresseDest;
		this.titre = titre;
		this.corps = corps;
		this.piecesJointes = new ArrayList<>();
	}

	
	
	//----- ACCESSEURS EN LECTURE ET ECRITURE --------
	
	public String getAdresseDest() {
		
		return adresseDest;
	}

	public void setAdresseDest(String adresseDest) {
		
		this.adresseDest = adresseDest;
	}

	public String getTitre() {
		
		return titre;
	}

	public void setTitre(String titre) {
		
		this.titre = titre;
	}

	public String getCorps() {
		
		return corps;
	}

	public void setCorps(String corps) {
		
		this.corps = corps;
	}

	public ArrayList<String> getPiecesJointes() {
		
		return piecesJointes;
	}

	public void setPiecesJointes(ArrayList<String> piecesJointes) {
		
		this.piecesJointes = piecesJointes;
	}
	
	// ------- METHODES ---------
	
	public void ajouterPieceJointe(String piece){
		
		this.piecesJointes.add(piece);
	}
	
	public void envoyer() throws CourrierInvalideException {
		
		//on verifie si les conditions pour qu'un courrier soit valide sont respectées
	

		if(this.titre.isEmpty() || this.titre == null) throw new CourrierInvalideException("ERREUR: titre vide");
		
		String regex = "[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z]+\\.[a-zA-Z]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(adresseDest);
        if (!matcher.matches()) throw new CourrierInvalideException("ERREUR: adresse incorrecte");
        
		if(this.corps.isEmpty() || (!this.corps.contains("PJ") && !this.corps.contains("joint") && !this.corps.contains("jointe"))) throw new CourrierInvalideException("ERREUR: mots manquants dans le corps");
		if(this.piecesJointes.isEmpty()) throw new CourrierInvalideException("ERREUR: pas de pieces jointes");
	
		
		//on verifie que le mail n'a pas deux pieces joints identiques
		
		if(this.piecesJointes.size() > 1) {
			
			for(int i = 0; i < this.piecesJointes.size(); i++) {
				for(int j = 0; j < this.piecesJointes.size(); j++) {
					if(j != i && this.piecesJointes.get(i).equals(this.piecesJointes.get(j))) throw new CourrierInvalideException("ERREUR: Doublons dans les pieces jointes!");
				}
			}		
		}	
		
		//si tout est conforme on envoie le mail
		System.out.println("Mail envoyé");
	}
	
}
