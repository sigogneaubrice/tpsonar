package EKIP.TP3;

@SuppressWarnings("serial")
public class CourrierInvalideException extends Exception{

	public CourrierInvalideException(String errorMessage) {
		
		super(errorMessage);
	}
	
}
