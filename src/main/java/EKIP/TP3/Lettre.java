package EKIP.TP3;

public class Lettre extends ObjetPostal
{
	private boolean urgence;
	private static float tarifBase=0.5f;
	private static String typeObjetPostal="Lettre";
	
	public Lettre()
	{ 
		super();
		urgence=false;
	}  



	public Lettre(String origine, String destination, String codePostal, float poids, float volume,
			Recommandation tauxRecommandation, boolean urgence) {
		super(origine, destination, codePostal, poids, volume, tauxRecommandation); // appel au constructeur de la super classe
		this.urgence = urgence;
	}



	//accesseurs

	public boolean isUrgence() {return urgence;}  

	//autres methodes

	public float getTarifBase(){return tarifBase;}

	public float tarifRemboursement()
	{if (getTauxRecommandation()==Recommandation.un) return 1.5f;
	else if (getTauxRecommandation()==Recommandation.deux) return 15;
	else return 0;
	}

	// redéfinition : spécialisation de la méthode tarifAffranchissement de la super classe
	@Override
	public float tarifAffranchissement()
	{
		float t=super.tarifAffranchissement();
		if (isUrgence())
			t = t+0.3f;
		return t;
	}

	@Override
	public String toString()
	{String s = super.toString()+"/";
	if (isUrgence()) s += "urgence";
	else s += "ordinaire";
	return s;
	}

	public String typeObjetPostal() {return typeObjetPostal;}


}
