package EKIP.TP3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestColis {

	
//============ initialisation des objets et variables statiques =========
	
	private static float tolerancePrix=0.001f;
	
	private Colis colis1; 
	
	private boolean alreadyInit = false;

	
	@BeforeEach
	public void initAll() {
		
		if(alreadyInit) return;
		
		else {
		
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
		
		alreadyInit = true;
		}
	}
	

//=============== Les differents tests ======================	
	
	@Test
	void testToString() {
	
		assertEquals(colis1.toString(), "Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0");
	}
	
	@Test
	void testAffranchissement() {
	
		assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);	
	}
	
	@Test
	void testRemboursement() {
	
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);	
	}
	
	@Test
	void testColisExpressTropLourd() {
		
		assertThrows(ColisExpressInvalide.class, () -> {
			
			new ColisExpress("Paris", "Montpellier", "34080", 33, 100, Recommandation.deux, "calendrier de l'avent", 200, true);
		});
	}
	
}
