package EKIP.TP3;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;


public class TestCourriel {


	//on initialise les objets et variables statiques
	
	private Courriel CourrielConforme;
	private Courriel CourrielAdresseIncorrecte;
	private Courriel CourrielTitreVide;
	private Courriel CourrielCorpsMotsManquants;
	private Courriel CourrielPasDePiecesJointes;
	private Courriel CourrielPiecesJointesDoubles;
	private Courriel CourrielTropLong;
	
	private boolean alreadyInit = false;
	
	@BeforeEach
	public void initAll() {
		
		if(alreadyInit) return;
		
		else {
			
			
			alreadyInit = true;
		}
	}
	

//=======================================================	

	//test sur un courrier classique conforme
	
	@Test
	void testCourrielConforme() {
		
		CourrielConforme = new Courriel("test@umontpellier.fr", "le titre", "ceci est le corps avec le mot PJ");
		CourrielConforme.ajouterPieceJointe("pieceJointe1");
		CourrielConforme.ajouterPieceJointe("pieceJointe2");
		
		try {
			CourrielConforme.envoyer();
			assertTrue(true);
		}catch(CourrierInvalideException exception) {
			
			System.out.println("Cette exception n'aurait pas du etre levée!");
		}
	}
	
	
//=======================================================

	
//=======================================================
	
	/** 
	* 
	* Test si le courrier n'a pas de titre
	*
	*/
	
	@Test
	void testCourrielTitreVide() {
		
		CourrielTitreVide = new Courriel("test@etu.umontpellier.fr", "", "ceci est le texte avec le mot jointe");
		CourrielTitreVide.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielTitreVide.envoyer();
		});
	}
	
//=======================================================
	
	/** 
	* 
	* Test si le courrier n'a pas les mots demandé dans son corps de texte
	*
	*/
	
	@Test
	void testCourrielCorpsMotsManquants() {
		
		CourrielCorpsMotsManquants = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le texte sans les mots demandés");
		CourrielCorpsMotsManquants.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielCorpsMotsManquants.envoyer();
		});
	}
	

//=======================================================
	
	/** 
	* Test si le courrier n'a pas de pieces jointes
	*/
	
	@Test
	void testCourrielPasDePiecesJointes() {
		
		CourrielPasDePiecesJointes = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le texte avec le mot PJ");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielPasDePiecesJointes.envoyer();
		});
	}
	
	
//=======================================================	
	
	/** 
	 * 
	 * Test parametré pour l'adresse V1
	 * Recoit des adresses inscrites manuellements dans \@ValueSource
	 *
	 */

	@ParameterizedTest
	@ValueSource(strings = { "34montpellier@.ui", "hellogmail.com", "@hotmail.com" })
	void testCourrielAdresseIncorrecteV1(String adresseTempo) {

		CourrielAdresseIncorrecte = new Courriel(adresseTempo, "le titre", "ceci est le corps avec le mot joint");
		CourrielAdresseIncorrecte.ajouterPieceJointe("pieceJointe1");

		assertThrows(CourrierInvalideException.class, () -> {

			CourrielAdresseIncorrecte.envoyer();
		});
	}


	/** 
	 * 
	 * Test parametré pour l'adresse V2
	 * Recoit des adresses venant de la fonction addressGenerator
	 *
	 */

	@ParameterizedTest
	@MethodSource("addressGenerator")
	void testCourrielAdresseIncorrecteV2(String adresseTempo) {

		CourrielAdresseIncorrecte = new Courriel(adresseTempo, "le titre", "ceci est le corps avec le mot joint");
		CourrielAdresseIncorrecte.ajouterPieceJointe("pieceJointe1");

		assertThrows(CourrierInvalideException.class, () -> {

			CourrielAdresseIncorrecte.envoyer();
		});
	}


	/** 
	 * 
	 * Fonction qui renvoie un flux aléatoire contenant un nombre fixé d'adresses volontairements fausses	
	 *
	 */

	public static Stream<String> addressGenerator() {

		ArrayList<String> addressContainer = new ArrayList<String>();

		int nbrAdressesTest = 3;

		//on va generer tant d'addresse et les ajouter a l'array
		for(int i = nbrAdressesTest; i > 0; i--) {

			//generation alea [0, 4] pour generer 5 types d'addresses invalides
			int alea = (int)Math.random() * (4 - 0 + 1) + 0;

			switch(alea) {

			case 0:

				addressContainer.add("@etu.umontpellier.fr");
				break;

			case 1:

				addressContainer.add("testEtu.umontpellier.fr");
				break;

			case 2:

				addressContainer.add("test@564.umontpellier.fr");
				break;

			case 3:

				addressContainer.add("test@etu.umontpellierFr");
				break;

			case 4:

				addressContainer.add("test@etu.umontpellier.");
				break;
			}	
		}

		return addressContainer.stream();
	}
		
	
	
	

//=========================================================	
	
	/**
	 * Courrier avec deux pieces jointes identiques
	 */
	
	@Test
	void testCourrierPiecesJointesDouble() {
		
		CourrielPiecesJointesDoubles = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le texte avec le mot PJ");
		CourrielPiecesJointesDoubles.ajouterPieceJointe("pieceJointe1");
		CourrielPiecesJointesDoubles.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielPiecesJointesDoubles.envoyer();
		});
	}
	
	@Test
	@Disabled("Pas important")
	void testCourrielTexteTropLong() {
		
		CourrielTropLong = new Courriel("test@etu.umontpellier.fr", "le titre", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. PJ");
		CourrielTropLong.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielTropLong.envoyer();
		});
	}
	
}