package EKIP.TP3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestLettre {

	private static float tolerancePrix=0.001f;
	
	private Lettre lettre1;
	private Lettre lettre2;

	private boolean alreadyInit = false;
	
	//initialisation des objets avec un beforeEach 
	//(mais qu'on parametrera pour qu'il ne se lance qu'une seule fois
	//BeforeAll etant une methode static on ne pourra pas modifier les attributs d'instances
	//a moins qu'on declare ces memes objets en static (ce qui n'est pas recommandé)
	
	@BeforeEach 
	public void initAll() {
		
		if(alreadyInit) return;
		
		else {
			
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		
		alreadyInit = true;
		}
	}
	
	
//=============== Les differents tests ======================
	
	@Test
	void testToString() {
		
		assertEquals(lettre1.toString(),"Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire");
		assertEquals(lettre2.toString(),"Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence");		
	}
	
	@Test
	void testAffranchissement() {
		
		assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);	
		assertTrue(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix);
	}
	
	@Test
	void testRemboursement() {
	
		assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
		assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	}
}
