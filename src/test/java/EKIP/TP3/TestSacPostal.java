package EKIP.TP3;

import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSacPostal {

	//initialisation des objets et variables statiques
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	private SacPostal sac1;
	private Lettre lettre1;
	private Lettre lettre2;
	private Colis colis1;
	SacPostal sac2;
	
	private boolean alreadyInit = false;
	
	@BeforeEach
	public void initAll() {
		
		if(alreadyInit) return;
		
		else {
		
		sac1 = new SacPostal();
		
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);

		
		alreadyInit = true;
		}
	}
	
	
//=============== Les differents tests ======================
	 
	
	@Test
	void TestRemboursement() {
		
		assertTrue(Math.abs(sac1.valeurRemboursement()-116.5f)<tolerancePrix);
	}
	
	@Test
	void TestVolume() {
		
		assertTrue(Math.abs(sac1.getVolume()-0.025359999558422715f)<toleranceVolume);
		sac2 = sac1.extraireV1("7877");
		assertTrue(sac2.getVolume()-0.02517999955569394f<toleranceVolume);
	}
}
